from sdnotify import SystemdNotifier
from socketserver import ThreadingTCPServer
from signal import signal, SIGRTMIN
from threading import Event, Thread
from time import sleep
import os
import traceback
import sys

from collector import CSVCollector, CollectorManager
from stoppable import StoppableProcess
import server as srv

class Collector(StoppableProcess):
    """
    Collector logging various ADC inputs on disk.
    """
    def setup(self):
        self.__collectors = CollectorManager(CSVCollector)
        self.__collectors.start()

    def finish(self):
        self.__collectors.stop()

class Server(StoppableProcess):
    """
    Server feeding various ADC inputs to remote clients.
    """
    def start_server(self):
        """
        Thread target to launch the server without being
        blocked by the serve_forever() call.
        """
        addr = ("192.168.42.1", 8888)
        try:
            self.__server = ThreadingTCPServer(addr, srv.TCPHandler)
            self.__server.daemon_threads = True
            self.__server.serve_forever()
        except OSError:
            srv.collectors.stop()
            print(traceback.format_exc())
            sys.exit(1)
        
    def setup(self):
        srv.collectors.start()
        Thread(target=self.start_server).start()

    def finish(self):
        srv.collectors.stop()
        self.__server.shutdown()

class ProcessRestarter(StoppableProcess):
    """
    Process that watches a process of the Monitor class, 
    restarting it whenever it returns a failure exit code.
    """
    def __init__(self, name):
        super().__init__()
        self.name = name

    def loop(self):
        if Monitor.exitcode(self.name) not in (None, 0):
            Monitor.start(self.name)
        sleep(1)

class Monitor:
    """
    Class managing the collector and server processes.

    It emulates systemd Restart=on-failure mechanism
    with the ProcessRestarter class.
    """
    # Processes
    __p = {}
    __p["collector"] = Collector()
    __p["server"] = Server()

    # Process restarters
    __r = {name: ProcessRestarter(name) for name in __p.keys()}
    
    @classmethod
    def start(cls, name):
        """
        Starts a new instance of the process with
        the given name if it is not already started,
        as well as its corresponding ProcessRestarter.
        """
        if not cls.__p[name].is_alive():
            cls.__p[name] = cls.__p[name].__class__()
            cls.__r[name] = ProcessRestarter(name)
            cls.__p[name].start()
            cls.__r[name].start()

    @classmethod
    def stop(cls, name):
        """
        Stops the process with the given name if it is already started,
        as well as its corresponding ProcessRestarter.
        """
        if cls.__p[name].is_alive():
            cls.__p[name].stop()
            cls.__p[name].join()
            cls.__r[name].stop()
            cls.__r[name].join()

    @classmethod
    def exitcode(cls, name):
        """
        Returns the exitcode of the process with the given name.
        """
        return cls.__p[name].exitcode

def sighandler(f, *args, **kwargs):
    """
    Returns the signal handler calling f with specified arguments.
    """
    def handler(signum, frame):
        f(*args, **kwargs)
    return handler

if __name__ == "__main__":
    with open("/run/monitor.pid", "w") as pidfile:
        pidfile.write(str(os.getpid()) + "\n")

    signal(SIGRTMIN+0, sighandler(Monitor.start, "collector"))
    signal(SIGRTMIN+1, sighandler(Monitor.start, "server"))
    signal(SIGRTMIN+2, sighandler(Monitor.stop, "collector"))
    signal(SIGRTMIN+3, sighandler(Monitor.stop, "server"))

    SystemdNotifier().notify("READY=1")

    Event().wait()
