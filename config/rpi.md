# WindTurbine Raspberry Pi configuration guide

**Notes**

- This guide assumes that the setup is done from a **GNU/Linux** equiped computer on a **Raspberry Pi B+ rev 1.2**. Setup should still be possible from another OS, but some operations may need alternative tools. For other revisions of the RPi, look for the compatible Arch Linux ARM release on [the official website](https://archlinuxarm.org/about/downloads).
- All operations will be executed as `root` user.
- Relative paths refer to this guide repository tree, whereas absolute paths refer to the RPi filesystem. Transfers between the two filesystems can be done over SSH with the `scp` command.

## Preparing the system

- Install the latest Arch Linux ARMv6 image following [this guide](https://archlinuxarm.org/platforms/armv6/raspberry-pi)
- Copy the netctl profile *config/etc/eduroam* into */etc/netctl* to have internet access on the RPi
- To configure the RPi over a direct ethernet SSH connection, setup a static IP address by replacing `DHCP=ipv4` with `Address=<IP-address>` in */etc/systemd/network/eth0.network*. When the RPi has booted, connect with `ssh alarm@<IP-address>` (password `alarm`). Root SSH login can be enabled by adding the line `PermitRootLogin yes` in */etc/ssh/sshd_config*. Also, it can be convenient to setup a passwordless SSH connection following [this guide](https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md).

## Into the Pi

- Once booted and logged into the RPi, setup the internet connection by starting the `eduroam` profile:
```
netctl start eduroam
```
- Then update the system:
```
pacman -Syu
```
- Optionally, hostname can be set to WindTurbine by editing */etc/hostname*

### Wi-Fi software access point

#### Driver

We first need to install a custom driver for the 8192 chip that supports AP mode (for more information, refer to [this guide](https://github.com/oblique/create_ap/blob/master/howto/realtek.md)).

- Install some needed packages:
```
pacman -S base-devel linux-headers dkms git
```
- Download and install the driver:
```
git clone https://github.com/pvaret/rtl8192cu-fixes.git
dkms add rtl8192cu-fixes
dkms install 8192cu/$(sed -n 's/PACKAGE_VERSION="\(.*\)"/\1/p' rtl8192cu-fixes/dkms.conf)
cp rtl8192cu-fixes/blacklist-native-rtl8192.conf /etc/modprobe.d
cp rtl8192cu-fixes/8192cu-disable-power-management.conf /etc/modprobe.d
rm -rf rtl8192cu-fixes
```
- Download and install a patched version of `hostapd` from the AUR:
```
pacman -S wget
wget https://aur.archlinux.org/cgit/aur.git/snapshot/hostapd-rtl871xdrv.tar.gz
tar -xvf hostapd-rtl871xdrv.tar.gz
cd hostapd-rtl871xdrv && makepkg
pacman -U hostapd-rtl871xdrv*.tar.xz
cd .. && rm -rf hostapd-rtl871xdrv*
```

#### create_ap

We use the bash script [`create_ap`](https://github.com/oblique/create_ap) to easily setup the AP.

- Install remaining dependencies needed by `create_ap`:
```
pacman -S dnsmasq
```
- Clone the git repository and install `create_ap`:
```
git clone https://github.com/oblique/create_ap
cd create_ap
make install
```
- Copy the `systemd` service *config/etc/create\_ap.service* in */etc/systemd/system*, then enable it at boot time with:
```
systemctl enable create_ap
```

### Python collector and server

- Install Python 3 and the `RPi.GPIO` library to read values from the ADC, as well as the `sdnotify` library to work with `systemd`:
```
pacman -S python python-pip
pip install RPi.GPIO sdnotify
```
- Clone the git repository of the server in the home directory:
```
git clone https://bitbucket.org/itt_dub_blag/board ~
```
- Copy the `systemd` units (`.service` and `.timer` files) from *config/etc/* in */etc/systemd/system*, then enable some of them at boot time with:
```
systemctl enable {collector,server,storage-full}.service storage-full.timer
```

## Epilogue

Now everything should run automatically at boot, you just have to connect to the AP from an Android device and start using the Monitor app!
