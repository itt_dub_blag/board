# WindTurbine Intel Edison configuration guide

**Notes**

- This guide assumes that the setup is done from a **GNU/Linux** equiped computer on a **Intel Edison kit for Arduino**. Setup should still be possible from another OS, but some operations may need alternative tools.
- To do the basic setup of the board and learn how to configure it over SSH, please refer to [this guide](https://learn.sparkfun.com/tutorials/edison-getting-started-guide).

### Wi-Fi software access point

This is pretty straightforward: just switch to AP mode by pressing the PWR button during 4-7 seconds, and you should see a Wi-Fi network sharing the name and the root password of the board.
The switch is still effective after reboot, and the IP address of the board is already correctly configured.

### Python collector and server

- Install Python 3 from source:
    - Download the latest version from [the official source](https://www.python.org/downloads/source/)
    - Copy the archive to the Edison
    - Install it with the following commands, replacing `<version>` with your version number:
    ```
    tar xvf Python-<version>.tar.xz
    cd Python-<version>
    ./configure
    make -j2
    make install
    ln -s /usr/local/bin/python3 /usr/bin/python3
    cd .. && rm -rf Python-<version>*
    ```
- Install the Python 3 version of the `mraa` library from source to read values from the ADC:
    - Download the latest version from [GitHub](https://github.com/intel-iot-devkit/mraa/archive/master.zip)
    - Copy the archive to the Edison
    - Install it with the following commands:
    ```
    unzip mraa-master.zip
    cd mraa-master
    mkdir build && cd build
    cmake ..
    make -j2
    make install
    cd ../.. && rm -rf mraa-master*
    ```
- Install the `sdnotify` library from source to work with `systemd`:
    - Download the latest version from [PyPI](https://pypi.python.org/pypi/sdnotify)
    - Copy the archive to the Edison
    - Install it with the following commands, replacing `<version>` with your version number:
    ```
    tar xvf sdnotify-<version>.tar.gz
    cd sdnotify-<version>
    python3 setup.py install
    cd .. && rm -rf sdnotify-<version>*
    ```
- Update /etc/ld.so.conf for Python to find the newly installed libraries:
```
echo "/usr/local/lib" >> /etc/ld.so.conf
ldconfig
```
- Install the `pkill` command to kill process using PID file:
    - Download the latest version from [SourceForge](https://sourceforge.net/projects/procps-ng/)
    - Copy the archive to the Edison
    - Install it with the following commands, replacing `<version>` with your version number:
    ```
    tar xvf procps-ng-<version>.tar.xz
    cd procps-ng-<version>
    ./configure
    make -j2
    make install
    ln -s /usr/local/usr/bin/pkill /usr/bin/pkill
    cd .. && rm -rf procps-ng-<version>*
    ```
- Clone the git repository of the server on your computer and then copy all the files in the home directory of the Edison:
```
git clone https://bitbucket.org/itt_dub_blag/board
scp board/* root@Edison:~
```
- Copy the `systemd` units (`.service` and `.timer` files) from *config/etc/* in */etc/systemd/system*, then enable some of them at boot time with:
```
systemctl enable {collector,server,storage-full}.service storage-full.timer
```
- Finally, create an alias to to the home directory:
```
ln -s /home/root /root
```

## Epilogue

Now everything should run automatically at boot, you just have to connect to the AP from an Android device and start using the Monitor app!
