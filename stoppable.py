import threading
import multiprocessing

class Stoppable:
    """
    Base class for stoppable instruction sets.

    It works by having a main loop exiting when the stop event is triggered.
    """
    def __init__(self, EventClass):
        self.__stop = EventClass()

    def setup(self):
        """
        Code to be executed before the main loop.
        """
        pass
    
    def loop(self):
        """
        Code to be executed inside the main loop.
        Defaults to waiting for the stop event to be triggered.
        """
        self.__stop.wait()

    def finish(self):
        """
        Code to be executed after the main loop.
        """
        pass

    def stop(self):
        """
        Triggers the stop event.
        """
        self.__stop.set()

    def run(self):
        self.setup()
        while not self.__stop.is_set():
            self.loop()
        self.finish()

class StoppableThread(Stoppable, threading.Thread):
    """
    Thread that can be stopped properly through the Stoppable interface.
    """
    def __init__(self, *args, **kargs):
        Stoppable.__init__(self, threading.Event)
        threading.Thread.__init__(self, *args, **kargs)

class StoppableProcess(Stoppable, multiprocessing.Process):
    """
    Process that can be stopped properly through the Stoppable interface.
    """
    def __init__(self, *args, **kargs):
        Stoppable.__init__(self, multiprocessing.Event)
        multiprocessing.Process.__init__(self, *args, **kargs)
