from multiprocessing import Lock
import sys

# One read operation from the ADC at a time
readlock = Lock()

# Adapt to current platform
try:
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    platform = "rpi"
except ImportError:
    try:
        import mraa
        platform = "edison"
    except ImportError:
        print("Error: the server must run on a Raspberry Pi or Intel Edison board.")
        sys.exit(2)

class ChannelException(Exception):
    def __init__(self, ):
        super().__init__(message)

class ADC:
    """
    Class representing a generic, readable ADC.

    Using this class directly will most likely lead to errors,
    as it is designed as an abstract class for real ADCs.
    """
    def init(resolution, vref, channels):
        """
        Decorator acting as a static member initializer to be used by subclasses.

        resolution -- resolution in bits
        vref -- reference voltage in volts
        channels -- list of channel numbers available to read from
        """
        def wrap(cls):
            cls.resolution = resolution
            cls.vref = vref
            cls.channels = channels
            return cls
        return wrap

    @classmethod
    def check_channel(cls, channel):
        """
        Raises an exception if the given channel number is not available.
        Should be invoked at the beginning of the read method in subclasses.
        """
        if channel not in cls.channels:
            raise ChannelException("Invalid channel number " + str(channel))

    @classmethod
    def read(cls, channel):
        """
        Returns one digital value read from a given channel.
        """
        raise NotImplementedError("read() not implemented")

    @classmethod
    def digital_to_volts(cls, value):
        return value * cls.vref / float(2**cls.resolution - 1)


if platform == "rpi":
    @ADC.init(10, 3.3, range(0, 7))
    class MCP3008(ADC):
        """
        The MCP3008 ADC used with the Raspberry Pi, with manual SPI transactions.
        """
        CLK = 11
        MISO = 9
        MOSI = 10
        CS = 8

        GPIO.setmode(GPIO.BCM)

        GPIO.setup(MOSI, GPIO.OUT)
        GPIO.setup(MISO, GPIO.IN)
        GPIO.setup(CLK, GPIO.OUT)
        GPIO.setup(CS, GPIO.OUT)

        @classmethod
        def read(cls, channel):
            with readlock:
                cls.check_channel(channel)

                GPIO.output(cls.CS, True)

                GPIO.output(cls.CLK, False)
                GPIO.output(cls.CS, False)

                commandout = channel
                commandout |= 0x18
                commandout <<= 3
                for i in range(5):
                    if commandout & 0x80:
                        GPIO.output(cls.MOSI, True)
                    else:
                        GPIO.output(cls.MOSI, False)
                    commandout <<= 1
                    GPIO.output(cls.CLK, True)
                    GPIO.output(cls.CLK, False)

                adcout = 0

                for i in range(12):
                    GPIO.output(cls.CLK, True)
                    GPIO.output(cls.CLK, False)
                    adcout <<= 1
                    if GPIO.input(cls.MISO):
                        adcout |= 0x1

                GPIO.output(cls.CS, True)

                adcout >>= 1
                return adcout

    ADC_CLASS = MCP3008

elif platform == "edison":
    @ADC.init(10, 3.3, range(0, 5))
    class ADS7951(ADC):
        """
        The ADS7951 ADC integrated in the Intel Edison Arduino breakout board.
        """
        @classmethod
        def read(cls, channel):
            with readlock:
                cls.check_channel(channel)
                aio = mraa.Aio(channel)
                aio.setBit(cls.resolution)
                return aio.read() + aio.readFloat()

    ADC_CLASS = ADS7951
