from socketserver import StreamRequestHandler
import subprocess
import traceback

from collector import CollectorManager, HistoryCollector

collectors = CollectorManager(HistoryCollector)

class TCPHandler(StreamRequestHandler):
    """
    Instantiated once per connection, handles a client connection,
    and sends data based on the requested command.

    A command is a two-characters string that should be defined as follows:
    - The first character correponds to the input source
    - The second character corresponds to the content requested

    To add a command behavior, simply define an instance method without 
    arguments named with the exact two-characters string of the command,
    and returning the response in bytes.
    """
    def handle(self):
        print("[" + self.client_address[0] + "]", "Connected")
        while True:
            try:
                cmd = self.request.recv(2).decode("utf8")
                # Empty byte means the client closed its socket
                if len(cmd) == 0:
                    print("[" + self.client_address[0] + "]", "Disconnected")
                    break
                # Log command
                print("[" + self.client_address[0] + "]", "Received:", cmd)
                # Build response
                data = getattr(self, cmd)()
                # Send data
                self.request.sendall(data + b"\0\n")
            except:
                print(traceback.format_exc())
                break

    # Battery commands

    # Current level
    def bl(self):
        sample = collectors["b"].sample()
        data = sample[0] + "," + str(sample[1]) + "\n"
        data = bytes(data, "utf8")
        return data

    # History
    def bh(self):
        data = ""
        with collectors["b"].lock:
            for sample in collectors["b"].history:
                data += sample[0] + "," + str(sample[1]) + "\n"
        data = bytes(data, "utf8")
        return data

    # Pressure sensors commands

    # Other commands

    # Storage full ?
    def sf(self):
        data = subprocess.run("/root/storage-full.sh").returncode
        data = bytes(str(data) + "\n", "utf8")
        return data
