from collections import deque
from threading import Lock
from time import time, strftime, sleep
import abc
import csv
import os

from adc import ADC_CLASS
from stoppable import StoppableThread

# ADC channel mappings, should be the common base for all ADCs.
# Key = channel number (e.g. 0 for battery)
# Value = source identifier (e.g. b for battery)
CHANNEL_IDS = {0: "b"}

# Path of the directory containing the CSV logs
LOGS_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "logs")

class Collector(StoppableThread, metaclass=abc.ABCMeta):
    """
    Thread collecting values from a specific channel of an ADC with a given sampling rate.
    """
    def __init__(self, adc, channel, sampling_rate):
        """
        adc -- ADC class to use
        channel -- channel of the ADC to read from
        sampling_rate -- sampling rate of the collector
        """
        super().__init__()

        self.adc = adc
        self.channel = channel
        self.sampling_rate = sampling_rate

    def sample(self, ndigits=3):
        """
        Samples a value from the ADC, and returns it as the second
        element of a tuple, along with its timestamp.

        ndigits -- precision of the value in decimal digits
        """
        timestamp = strftime("%Y-%m-%d %H:%M:%S")
        value = round(self.adc.digital_to_volts(self.adc.read(self.channel)), ndigits)
        return (timestamp, value)

    @abc.abstractmethod
    def collect(self, sample):
        """
        Defines how to collect a sample.
        """
        pass

    def loop(self):
        self.collect(self.sample())
        sleep(1 / self.sampling_rate)

class CollectorManager(dict):
    """
    Dictionary of Collector objects sharing common properties,
    with utility methods to manage them.

    A collector is accessed with its source identifier,
    e.g. self["b"] for the battery collector.
    """
    def __init__(self, CollectorClass, adc = ADC_CLASS, channel_ids = CHANNEL_IDS):
        """
        CollectorClass -- class of the collectors
        adc -- ADC class used by the collectors
        channel_ids -- {channel: source identifier} dictionary
        """
        self.CollectorClass = CollectorClass
        self.adc = adc
        for channel, id_ in channel_ids.items():
            self[id_] = CollectorClass(adc, channel)

    def add(self, id_, channel):
        """
        Adds a new collector to this manager.
        """
        self[id_] = self.CollectorClass(self.adc, channel)

    def start(self):
        """
        Starts all collectors.
        """
        for c in self.values():
            c.start()

    def stop(self):
        """
        Stops all collectors.
        """
        for c in self.values():
            c.stop()
        for c in self.values():
            c.join()

class HistoryCollector(Collector):
    """
    Collector handling a limited size history of the latest samples.
    """
    def __init__(self, adc, channel, sampling_rate=1/6, histsize=100):
        super().__init__(adc, channel, sampling_rate)
        self.history = deque(maxlen=histsize)
        self.lock = Lock() # Only one thread can use the history at a time

    def collect(self, sample):
        with self.lock:
            self.history.append(sample)

class CSVCollector(Collector):
    """
    Collector logging samples on disk using the CSVDateLogger class.
    """
    def __init__(self, adc, channel, sampling_rate=6):
        super().__init__(adc, channel, sampling_rate)
        self.csv = CSVDateLogger(LOGS_DIR)

    def collect(self, sample):
        self.csv.log(sample)

class CSVDateLogger:
    """
    A CSV logger automatically splitting logs into a file hierarchy,
    with one file per day, one directory per month, and one directory per year.
    """
    def __init__(self, path="logs"):
        """
        path -- path of the directory containing the file hierarchy
        """
        self.path = path
        os.makedirs(path, exist_ok=True)

    def log(self, row):
        self.update_filepath()
        with open(self.filepath, "a", newline="") as f:
            csv.writer(f).writerow(row)

    def update_filepath(self):
        dirpath = os.path.join(self.path, strftime("%Y"), strftime("%B"))
        os.makedirs(dirpath, exist_ok=True)
        self.filepath = os.path.join(dirpath, strftime("%d") + ".csv")
