#!/bin/bash

used_percentage=$(df / | tail -1 | tr -s " " | cut -d " " -f 5 | tr -d "%")
if [ $used_percentage -ge 95 ]; then
    systemctl stop collector &
    exit 1
fi
exit 0
